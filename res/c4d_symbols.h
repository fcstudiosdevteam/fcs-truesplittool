enum
{
	// End of symbol definition
	IDS_SMP_NAME					= 10000,
	IDS_SMP_DESC					= 10001,

	ID_SMP_SPLIT_WHOLE_OBJECT 		= 10002,
	ID_SMP_ABOUT 					= 10004,
	ID_SMP_HELP 					= 10005,
	ID_SMP_CENTER_AXIS 				= 10007,
	ID_SMP_APPLY_COLOR 				= 10008,
	ID_SMP_ATTACH_CHILD 			= 10009,
	ID_SMP_REMOVE_ALL_MAT_TAGS 		= 10011,
	ID_SMP_OPTIMIZE_SPLITOBJ 		= 10012,
	ID_SMP_OPTIMIZE_MAINOBJ 		= 10013,
	ID_SMP_DEL_POLY 				= 10014,
	ID_SMP_REMOVE_UNUSED_MATS		= 10015,
	ID_SMP_STAY_POLYMODE			= 10016,


  _DUMMY_ELEMENT_
};
