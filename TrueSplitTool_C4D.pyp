""" 
FCS TrueSplit Tool
_____________________________________________________________________
"""
FCSWEBLINKS= { 
                "OnlineManual":"https://docs.google.com/document/d/e/2PACX-1vTGCu4gX5fmblEzyE7hpyH1wxTLFgZkot_1AWVqoUGw4wv5KM71m1USABDukJxOb3vCVb-lb38N6ErK/pub",
                "DIS":"https://discord.gg/CQQn33W",  
                "GUM":"https://gumroad.com/fcstudios",
                "YT":"https://www.youtube.com/channel/UC_5YTyCTAFyNut_QR0YZqsg"
             }

Initialized = "TrueSplit v1.3 is Initialized. Tool by F.C.S"
AboutTool = """ 
            About

            Version : TrueSplit v1.3 RC 2020 \n\n Deleveloper : Ashton Rolle aka ApAshtonTheCreator \n Studio Company : Field Creators Studios (F.C.S)\n\nSpecial Thanks To: 

            Maxon Plugin Development Support Forums
            
            Samuel Winter/DigitalMeat3D : Naming of the Plugin
            
            Eric Bowers : Beta Tester

            """

""" 
Normal Imports Modules
_____________________________________________________________________
"""
import os
import sys
import subprocess
import webbrowser
import urllib
import glob
import struct
import time
import datetime
import collections 
from random import randint
import ast
# Regex Import
import re
# Json Import to store or load descriptions or any data / .json is easy and alot flexable and short coding.
import json

""" 
C4D Imports Modules
_____________________________________________________________________
"""
import c4d
from c4d import plugins, gui, bitmaps, documents, storage, utils, Matrix, Vector
from c4d.gui import GeUserArea as CustomUI



"""
Plugin Common Global Utilites Helpers
____________________________________________________________________
"""
# [ ID Generator ]
uiIDS = 0
SelectedDocPageName = None
BG_LitDarkBlue_Col = c4d.Vector(0.08203125, 0.1015625, 0.12109375)
BG_LightDarker_COL = c4d.Vector(0.125, 0.125, 0.125)
BG_BlueCOL = c4d.Vector(0.0078125, 0.72265625, 0.9765625)
BG_DARK = c4d.Vector(0.1015625, 0.09765625, 0.10546875)
DARK_BLUE_TEXT_COL = c4d.Vector(0, 0.78125, 0.99609375)
# [ ID Generator ]
def GuiIdGenerator(IDsList):
    """ ID Generator  """
    global uiIDS
    uiIDS+= 1
    ID = IDsList + uiIDS
    #print(ID)
    return ID
# [ Create a Bitmap Button Custom GUI ]
class CustomImageButtonGUI(object):
    """
    Create a Bitmap Button Custom GUI.

    `Code Example:`

    self.ImgButton = CustomImageButtonGUI(``self``)

    self.ImgButton.GUI(id=``1001``, ``**key_properties``)

    ` In Code : `

    self.ImgButton.GUI( ``[1001]``,  \<--------------------------------------| UI Widget Button [ID]

                        borderStyle = ``c4d.BORDER_IN`` ,   \<---------------|
                        icon =``IconPath`` ,                                \|**kwargs = **key_properties
                        backgroundCol = ``c4d.COLOR_TEXTFOCUS`` ,           \|These are key button properties that
                        clickable = ``True`` ,                              \|is for what you want the button
                        tip = ``"<b>HelloWorld</b>/nHi"`` ,                 \|to have. Basically customize the
                        toggleable = ``True``               \<---------------|the way you want it to be.
                       )
    """
    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get
        and passes data on from the another class also.
        """
        self.ui_instance = ui_instance
        super(CustomImageButtonGUI, self).__init__()

    # Bitmap Button Custom GUI.
    def GUI(self, ID, **key_properties):
        """
        `Code Example:`

        self.ImgButton = CustomImageButtonGUI(``self``)

        self.ImgButton.GUI(id=``1001``, ``**key_properties``)

        ` In Code : `

        self.ImgButton.GUI( ``[1001]``,  \<--------------------------------------| UI Widget Button [ID]

                            borderStyle = ``c4d.BORDER_IN`` ,   \<---------------|
                            icon =``IconPath`` ,                                \|**kwargs = **key_properties
                            backgroundCol = ``c4d.COLOR_TEXTFOCUS`` ,           \|These are key button properties that
                            clickable = ``True`` ,                              \|is for what you want the button
                            tip = ``"<b>HelloWorld</b>/nHi"`` ,                 \|to have. Basically customize the
                            toggleable = ``True``               \<---------------|the way you want it to be.
                        )
        """
        ui = self.ui_instance

        # Create GUI Button.
        CustomGui_UI = c4d.BaseContainer()                                  # Create a new container to store the button image.

        CustomGui_UI.SetFilename(ID, key_properties['icon'])                # Add this location info to the conatiner

        CustomGui_UI.SetLong(c4d.BITMAPBUTTON_BORDER, c4d.BORDER_NONE)

        CustomGui_UI.SetBool(c4d.BITMAPBUTTON_BUTTON, True)

        for setting, value in key_properties.items():                       # Adding settings from the dic that item setting is there in or added to it.

            if setting == 'borderStyle':
                BorderLook =  key_properties['borderStyle']
                CustomGui_UI.SetLong(c4d.BITMAPBUTTON_BORDER, BorderLook)   # Sets the border flag to look like a button look. eg.( c4d.BORDER_NONE or c4d.BORDER_THIN_OUT )

            elif setting == 'toggleable':
                # ToggleBTN = btn_settings['toggleable']
                CustomGui_UI.SetBool(c4d.BITMAPBUTTON_TOGGLE, True)         # Toggle button, like a checkbox.

            elif setting == 'clickable':
                CustomGui_UI.SetBool(c4d.BITMAPBUTTON_BUTTON, key_properties['clickable'])         # Clickable button.

            elif setting == 'backgroundCol':
                AddBackgroundColor = key_properties['backgroundCol']
                CustomGui_UI.SetInt32(c4d.BITMAPBUTTON_BACKCOLOR, AddBackgroundColor) # int - Background color.

            elif setting == 'tip':
                TipInfo = key_properties['tip']
                if TipInfo == "":
                    return
                else:
                    CustomGui_UI.SetString(c4d.BITMAPBUTTON_TOOLTIP, TipInfo) # Add tooltios.path. eg.[self.SetString(c4d.BITMAPBUTTON_TOOLTIP, "<b>Bold Text</b><br>New line")]

        ui.SetBTN = ui.AddCustomGui(ID, c4d.CUSTOMGUI_BITMAPBUTTON, "", c4d.BFH_MASK, 0, 0, CustomGui_UI)
        ui.SetBTN.SetImage(key_properties['icon'], True)
        return True
# [ Create a QuickTab Custom GUI ]
class AddCustomQuickTab_GUI(object):
    """
    This Custom GUI call a QuickTab is how to add and create a :
        - ``QuickTab Default Bar Title GUI``
        - ``QuickTab Fold Arrow Handle Bar Title GUI``
        - ``QuickTab Radio Tabs Bar Titles GUI ``
    """

    CUSTOMGUI_FLAG = c4d.CUSTOMGUI_QUICKTAB
    uiRadioTab = None

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance    

    def Add_BarTitle_GUI(self, bar_id, bar_name, width, height, ui_color):
        """ 
        Create a QuickTab Default Bar Title Custom GUI. 
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)
        if ui_color:
            self.instance.CustomGui_UI[c4d.QUICKTAB_BGCOLOR] = ui_color
        self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        return True   

    def Add_BarTitleArrowHandle_GUI(self, bar_id, bar_name, width, height, ui_color):
        """ 
        Create a QuickTab Fold Arrow Handle Bar Title Custom GUI. 
        """
        self.instance.CustomGui_UI = c4d.BaseContainer()
        self.instance.CustomGui_UI.FlushAll()
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, True)
        self.instance.CustomGui_UI.SetString(c4d.QUICKTAB_BARTITLE, bar_name)
        if ui_color:
            self.instance.CustomGui_UI[c4d.QUICKTAB_BGCOLOR] = ui_color
        # GUI Toggle System for QuickTab Bar Fold Icon Button.  
        # To Help with Toggle Mode of Open and Closing the Bar Grouos.path. / Its just a dummy to help with Toggling.
        # Handle as subgroup. Like bar mode, but with fold arrow icon. Implies QUICKTAB_BAR. Call QuickTabCustomGui.IsSelected()            
        self.instance.CustomGui_UI.SetBool(c4d.QUICKTAB_BARSUBGROUP, True)
         # Set the Fold Handle to Open or Close.
        self.instance.ui = self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, self.instance.CustomGui_UI)
        self.instance.ui.Select(0, True) # Set the Fold Handle to Open or Close.
        return True

    def Add_BarTabsRadio_GUI(self, bar_id, width, height, ui_color, list_tabs):
        """
        Create a QuickTab Radio Tabs Bar Titles Custom GUI.
        """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 1, 0, "")
        self.instance.GroupSpace(0, 0)
        CustomGui_UI = c4d.BaseContainer()
        CustomGui_UI.FlushAll()
        CustomGui_UI.SetBool(c4d.QUICKTAB_BAR, False)
        CustomGui_UI.SetBool(c4d.QUICKTAB_SHOWSINGLE, True)
        CustomGui_UI.SetBool(c4d.QUICKTAB_NOMULTISELECT, True)
        self.instance.uiRadioTab = self.instance.AddCustomGui(bar_id, self.CUSTOMGUI_FLAG, "", c4d.BFH_SCALEFIT, width, height, CustomGui_UI)
        for tab in list_tabs:
            self.instance.uiRadioTab.AppendString(tab['id'], tab['str'], tab['state'])
        if ui_color:
            # c4d.COLOR_QUICKTAB_BG_ACTIVE
            # c4d.COLOR_QUICKTAB_TEXT_ACTIVE 
            # c4d.COLOR_QUICKTAB_TEXT_INACTIVE
            # c4d.COLOR_QUICKTAB_BG_INACTIVE
            self.instance.uiRadioTab.SetDefaultColor(bar_id, c4d.COLOR_QUICKTAB_BG_ACTIVE, ui_color)       
        self.instance.GroupEnd()
        return self.instance.uiRadioTab

    def TabRadioIsSelected(self, widgetUI, selectedID):
        state = None
        if widgetUI.IsSelected(selectedID):
            state = True
        else:
            state = False
        return state
# [ Create a UI Window Dialog GUI Base Class ]
class BaseWindowDialogUI(c4d.gui.GeDialog):
    """ The Base Window GUI Dialog Class """

    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "Base Window"
    windowDialogWidthSize = None
    windowDialogHeightSize = None
    windowDialogPluginID = 0


    def BuildUI(self):
        """
        Creating Window UI Layout Sturcture.
        """
        ui = self
        pass

    def UIsettings(self):
        """ To set UI elements values for when the window is initialized. """
        ui = self
        pass

    def UIfunctions(self, id):
        """ Excuting Commands for UI Elements Functions. """
        ui = self
        pass

    def openWindowUI(self, winDlgType):
        """ To Open the UI window. """
        sWidth = self.windowDialogWidthSize 
        sHeight = self.windowDialogHeightSize         
        return self.Open(dlgtype=winDlgType, pluginid=self.windowDialogPluginID, xpos=-3, ypos=-3, defaultw=sWidth, defaulth=sHeight, subid=0)
    
    def restoreWindowUI(self, sec_ref):
        """ To Restore the UI window. """
        return self.Restore(pluginid=self.windowDialogPluginID, secret=sec_ref)

    def closeWindowUI(self):
        """ To CLOSE the UI window. """
        return self.Close()

    """
    Main C4D GeDialog GUI Class Overrides.
    _________________________________________________
    """
    def __init__(self):
        """
        The __init__ is an Constuctor and help get or set data
        into the class and passes data on from the another class.
        """
        self.TileBar = AddCustomQuickTab_GUI(self)
        self.ImgBtn = CustomImageButtonGUI(self)
        self.HtmlWin = CustomHTML_GUI(self)        
        super(BaseWindowDialogUI, self).__init__()

    def CreateLayout(self):
        """
        Window GUI elements layout that display to the User.
        """
        self.BuildUI()
        return True

    def InitValues(self):
        """
        Called when the dialog is initialized by the GUI / GUI's startup values basically.
        """
        #fcsLog.DEBUG("F.C.S"+ self.windowMainTileName +" is Open Now!", False, DebugMode)
        self.SetTitle(self.windowMainTileName)
        self.UIsettings()
        return True

    def Command(self, id, msg):
        """
        Excuting Commands for UI Elements Functions.
        """
        #if id:
            #fcsLog.INFO(str(id), False, DebugMode)        
        self.UIfunctions(id)
        return True

    def CoreMessage(self, id, msg):
        """
        Override this function if you want to react to Cinema 4D core messages.
        The original message is stored in msg
        """
        #if id == c4d.EVMSG_CHANGE:
        #    pass
        #self.UI_COREMESSAGES
        return True

    def DestroyWindow(self):
        """
        DestroyWindow Override this method - this function is called when the dialog is
        about to be closed temporarily, for example for layout switching.
        """
        #self.UI_CLOSEFUNCTION
        print( self.windowMainTileName + " Window Dialog Close.")
        pass
# [ Create a Custom Node Text Button GUI ]
class CustomStringTextNodeGUI(CustomUI):
    
    values = None

    # ---------------------------------------------------
    #    Custom Operations Hepler Method Functions
    # ---------------------------------------------------
    def GetMouseGlobalLocal(self, msg):
        x, y = msg.GetLong(c4d.BFM_INPUT_X), msg.GetLong(c4d.BFM_INPUT_Y)
        map_ = self.Global2Local()
        x += map_['x']
        y += map_['y']
        return x, y

    # ---------------------------------------------------
    #    Main c4d.gui.GeUserArea Operations Functions which
    #    are Methods to Override for GeUserArea class.
    # ---------------------------------------------------
    def __init__(self, ui_widget_id, DisplayStrText, State):
        """
        The __init__ is an Constuctor and help get
        and passes data on from another classes or
        function methods.
        """
        super(CustomStringTextNodeGUI, self).__init__()
        #print("GraphViewportLoaded")
        self.last_t = -1
        self.mouse_in = False
        self.interval = 0.2
        self.paramid = ui_widget_id
        self.DisplayStrText = DisplayStrText
        self.selected = State

    def DrawMsg(self, x1, y1, x2, y2, msg_ref):
        self.OffScreenOn()
        self.SetClippingRegion(x1, y1, x2, y2)
        # BackGround Color
        self.DrawSetPen(c4d.COLOR_BG) # -----> Set Color of the Draw Type or Shape Below
        self.DrawRectangle(x1, y1, x2, y2)
        # Text
        if self.selected:
            self.DrawSetTextCol(DARK_BLUE_TEXT_COL, c4d.COLOR_TRANS)
        else:
            self.DrawSetTextCol(c4d.Vector(0.99609375, 0.99609375, 0.99609375), c4d.COLOR_TRANS)
        self.DrawSetFont(c4d.FONT_BOLD)
        self.DrawText(" "+self.DisplayStrText, 0, 0)


    def InputEvent(self, msg):
        """
        Called when an input event is received.
        The information about the input event is stored in the msg container.
        See also: (Input Events).
        """
        device = msg.GetLong(c4d.BFM_INPUT_DEVICE)
        mouse_wheel = c4d.BFM_INPUT_MOUSEWHEEL
        mouse_right = c4d.BFM_INPUT_MOUSERIGHT
        channel = msg.GetLong(c4d.BFM_INPUT_CHANNEL)
        state = c4d.BaseContainer()
        catched = False

        # [ If the user Left click. ]
        if device == c4d.BFM_INPUT_MOUSE and channel == c4d.BFM_INPUT_MOUSELEFT:
            self.pressed = True
            catched = True
            # [ Poll the event. ]
            while self.GetInputState(device, channel, msg):
                if not msg.GetLong(c4d.BFM_INPUT_VALUE):
                    break
                x, y = self.GetMouseGlobalLocal(msg)
                if self.pressed:
                    #self.SendData(mBtn="Mouse Left Press", mouseposX=x, mouseposY=y)
                    self.pressed = False
                    #print("Mouse is in the out of viewport")
                    self.selected = True
                    # [ Invoke the dialogs Command() method. ]
                    actionmsg = c4d.BaseContainer(msg)
                    actionmsg.SetId(c4d.BFM_ACTION)
                    actionmsg.SetLong(c4d.BFM_ACTION_ID, self.paramid)
                    self.SendParentMessage(actionmsg)
        self.Redraw()
        return catched
def AddCustomNodeTextButton(ui_instance, id, text, w, l, state, auto_store=True):
    """ Creates an Custom Node Graph on the passed dialog. """
    #ui_instance._icon_buttons = []
    user_aera_class = CustomStringTextNodeGUI(id, text, state)
    if auto_store:
        if not hasattr(ui_instance, '_icon_buttons'):
            ui_instance._icon_buttons = []
        ui_instance._icon_buttons.append(user_aera_class)

    ui_instance.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 2, 0, "")
    ui_instance.GroupSpace(0, 0)

    grpid = GuiIdGenerator(IDsList=200000)
    ui_instance.GroupBegin(grpid, c4d.BFH_MASK, 1, 0, "")
    ui_instance.AddStaticText(0, c4d.BFH_MASK, 7, l, "")
    ui_instance.GroupEnd()
    ui_instance.SetDefaultColor(grpid, c4d.COLOR_BG, c4d.Vector(0.9765625, 0.26171875, 0.26171875))

    ui_instance.AddUserArea(id, c4d.BFH_MASK, w, l)
    ui_instance.AttachUserArea(user_aera_class, id)

    ui_instance.GroupEnd()

    return user_aera_class
# [ Create HTML Viewer GUI ]
class CustomHTML_GUI(object):

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get
        and passes data on from the another class also.
        """
        self.ui_instance = ui_instance
        super(CustomHTML_GUI, self).__init__()

    def GUI(self, ui_widget_id, page_path):
        ui = self.ui_instance
        ui.GroupBegin(0, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 1, 0, "")
        CustomGui_UI = c4d.BaseContainer()
        html = ui.AddCustomGui(ui_widget_id, c4d.CUSTOMGUI_HTMLVIEWER, "", c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 0, 0, CustomGui_UI)
        html.SetUrl(page_path, c4d.URL_ENCODING_ESCAPED)
        ui.GroupEnd() 
        return True
    
    def HtmlViewerUpdater(self, ui_widget_id, page_path):
        ui = self.ui_instance
        CustomGui_UI = c4d.BaseContainer()
        html = ui.FindCustomGui(ui_widget_id, c4d.CUSTOMGUI_HTMLVIEWER)
        html.SetUrl(page_path, c4d.URL_ENCODING_ESCAPED)    
        html.DoAction(c4d.WEBPAGE_REFRESH)
        return True
# [ Create ProgressBar GUI ]
class AddCustomProgressBar_GUI(object):
    """
    # Progress Bar GUI 

    # Add a Custom Progress Bar GUI Layout 

    # Code Example:
    
    self.CUSTOM_PROGRESSBAR = AddCustomProgressBar_GUI(ui_instance=self)
    
    # Note: If you dont want a string amount UI display then set (str_amount_id=None).
    self.CUSTOM_PROGRESSBAR.GUI(str_amount_id=id, 
                                progress_id=id, 
                                size_w=190, 
                                size_l=10 )

    # Note: If you dont want a custom color then set (col=None).                            
    self.CUSTOM_PROGRESSBAR.Run_ProgressBar(progressbar_ui_id=id, 
                                           currentItemNum=1, 
                                           amountOfItems=(Amount of Items eg:10), 
                                           col=g_lib.DARK_BLUE_TEXT_COL)  # DARK_BLUE_TEXT_COL # DARK_RED_TEXT_COL 
                                                                            color is a c4d.Vector(0,0,0). 
                                                                            eg: DARK_RED_TEXT_COL=c4d.Vector(0.99609375, 0, 0)

    self.CUSTOM_PROGRESSBAR.Stop_ProgressBar(progressbar_ui_id=id)
    """

    def __init__(self, ui_instance):
        """
        The __init__ is an Constuctor and help get 
        and passes data on from the another class.
        """
        self.instance = ui_instance    

    def GUI(self, str_amount_id, progress_id, size_w, size_l):
        """ Progress Bar GUI Layout """
        self.instance.GroupBegin(0, c4d.BFH_SCALEFIT, 3, 0, "")
        self.instance.GroupBorderNoTitle(c4d.BORDER_THIN_IN)
        # ProgressBar
        self.instance.AddCustomGui(progress_id, c4d.CUSTOMGUI_PROGRESSBAR, "", c4d.BFH_SCALEFIT, size_w, size_l)
        if str_amount_id:
            # Static UI Text
            self.instance.AddSeparatorV(0, c4d.BFV_SCALEFIT)
            self.instance.AddStaticText(str_amount_id, c4d.BFH_MASK, 50, size_l, " 0%", c4d.BORDER_WITH_TITLE_BOLD)
        self.instance.GroupEnd()
        return True

    def Run_ProgressBar(self, str_amount_id, progressbar_ui_id, currentItemNum, amountOfItems, col):
        """ Progress Bar Running  """
        percent = float(currentItemNum)/amountOfItems*100
        # Set Data to PROGRESSBAR
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg[c4d.BFM_STATUSBAR_PROGRESSON] = True
        progressMsg[c4d.BFM_STATUSBAR_PROGRESS] = percent/100.0 
        # this if you want a custom color
        if col:
            self.instance.SetDefaultColor(progressbar_ui_id, c4d.COLOR_PROGRESSBAR, col)    
        self.instance.SendMessage(progressbar_ui_id, progressMsg)
        # Return Percent String Data
        PercentData = str(int(percent))+"%"
        return self.instance.SetString(str_amount_id, " "+PercentData)
    
    def Stop_ProgressBar(self, progressbar_ui_id):
        """ Progress Bar Stop Running  """
        progressMsg = c4d.BaseContainer(c4d.BFM_SETSTATUSBAR)
        progressMsg.SetBool(c4d.BFM_STATUSBAR_PROGRESSON, False)
        self.instance.SendMessage(progressbar_ui_id, progressMsg)
        return True

"""
Plugin Extra Utilites Helpers
____________________________________________________________________
"""
class FCS_JsonSystem_Editor():
    
    """
    FCS Json Editor System
    ``Eg:``
    jsonEdit = g_lib.FCS_JsonSystem_Editor()
    jsonEdit.GetItemProperties(json_file=FILE, main_property="TOOLS", itemData"Tool1")
    """

    def SaveFile(self, json_file, saveData):
        """ Save and Create .Json File. """
        try:
            with open(json_file,'w') as f:
                json.dump(saveData, f, indent=2, sort_keys=True)
        except Exception as e:
            gui.MessageDialog(e)
            return False
        return True

    def LoadFile(self, json_file):
        """ Load and Read .json file. """
        try:
            with open(json_file, 'r') as Jfile:
                return json.load(Jfile)
        except Exception as e:

            return None

        return True

    def PrintAllObjects(self, json_file):
        """ Get all Items .json file. """
        print("======| Main Items |======")
        jsonFileData = self.LoadFile(json_file)

        if jsonFileData == None:
            return False

        for i in jsonFileData:
            print(i)
        return True

    def GetItemProperty(self, json_file, main_property):
        """ Getting the main item property values. """
        jsonFileData = self.LoadFile(json_file)

        if jsonFileData == None:
            return None

        try:
            if jsonFileData[main_property]:
                print(jsonFileData[main_property])
                #gui.MessageDialog(jsonFileData[main_property])
                return jsonFileData[main_property]
        except KeyError:
            return gui.MessageDialog("Key doesn't exist ({0})".format(main_property))
        return True

    def GetItemProperties(self, json_file, main_property, itemData):
        """ Getting the child item property values of a main item property. """
        jsonFileData = self.LoadFile(json_file)

        if jsonFileData == None:
            return None

        for key, value in jsonFileData.iteritems():
            try:
                if key == main_property:
                    #print(value[itemData])
                    #gui.MessageDialog(value[itemData])
                    return value[itemData]
            except KeyError:
                return #print "Key doesn't exist"
        return True

    def GetItem(self, json_file, childPropertyName):
            """ Getting the child item property values of a main item property. """

            try:
                data = None

                jsonFileData = self.LoadFile(json_file)

                if jsonFileData == None:
                    return None

                if childPropertyName in jsonFileData:
                    data = jsonFileData[childPropertyName]

                if data == None:
                    print(type(jsonFileData))

                    if isinstance(jsonFileData, dict):
                        data = self.FindPropertyInDict(childPropertyName, jsonFileData)

                return data

            except KeyError as ke:
                print(ke)
                return False

            except Exception as e:
                print(e)
                return False
            return True

    def FindPropertyInDict(self, propertyName, jsonFileData):
        for key, value in jsonFileData.iteritems():
            if(key == propertyName):
                return jsonFileData

            if isinstance(value, dict):
                result = self.FindPropertyInDict(propertyName, value)
                return result
        return True

    def JsonBoolConverter(self, data):
        """ Bool Checker """
        value = None
        if data == True:
            value = "True"
        else:
            value = "False"
        return value

    def ArgumentFix(self, json):
        return json.replace("\"", "**",)

    def ConvertToJson(self, c4dData):
        jsonFile = self.ArgumentFix(json.dumps(c4dData.__dict__))
        return jsonFile

    def GetItemArrayOfProperties(self, main_property, json_file):
        print("======| "+ main_property +" All Items Names |======")
        jsonFileData = self.LoadFile(json_file)
        for stores in jsonFileData[main_property]:
         for item in stores:
           # Print my results
            print(item)
        return True

    def EditItemProperty(self, json_file, main_property, new_value):
        """ Editing a main property value. """
        jsonFileData = self.LoadFile(json_file)
        jsonFileData[main_property] = new_value
        self.SaveFile(json_file, jsonFileData)
        return True

    def EditItemProperties(self, json_file, main_property, child_property, new_value):
        """ Editing a properties child values. """
        jsonFileData = self.LoadFile(json_file)
        data = jsonFileData[main_property]
        data[child_property] = new_value
        self.SaveFile(json_file, jsonFileData)
        return True

    def DeletePropertyItem(self, json_file, property_name):
        """  """
        jsonFileData = self.LoadFile(json_file)
        try:
            if jsonFileData[property_name]:
                del jsonFileData[property_name]
        except KeyError:
            return #print "Key doesn't exist"
        self.SaveFile(json_file, jsonFileData)
        return True

    def DeleteChildPropertyItem(self, json_file, main_property, property_name):
        """  """
        jsonFileData = self.LoadFile(json_file)
        data = jsonFileData[main_property]
        try:
            if data[property_name]:
                del data[property_name]
        except KeyError:
            return #print "Key doesn't exist"
        self.SaveFile(json_file, jsonFileData)
        return True

    def BoolCheckerHelper(self, data):
        """ Json Bool Converter """
        value = None
        if ast.literal_eval(data):
            value = True
        else:
            value = False
        return value
class DirectoryHepler(object):

    def IsDirectoryVaild(self, DIR):
        """ Check If Directory is There or Not. And (rv) means ``ReturnValue.`` """
        rv = None
        if not os.path.exists(DIR):
            rv = None
        else:
            rv = DIR
        return rv

    def GetAllFilesInDirectory(self, DIR, LISTOFFILES, Extension=None):
        """ 
        Getting and Looking for all files in folder. and this method returns the list of files ``LISTOFFILES``.
        For Extension Att.. is for what type of file extentsion you only want to collect out of the dir folder. ``Eg: "*".txt``.
        ``CodeEg: GetAllFilesInDirectory(DIR=[YourPath], LISTOFFILES=[YourList], Extension=["*".txt])``
        """   
        os.chdir(DIR)
        for each_file in glob.glob(Extension):
            LISTOFFILES.append(each_file)
        return True

    def GetAllFilesInDirectoryAsList(self, DIR, Extension=None):
        """ 
        V2 (More Cleaner)
        Getting and Looking for all files in folder. and this method returns the list of files ``LISTOFFILES``.
        For Extension Att.. is for what type of file extentsion you only want to collect out of the dir folder. ``Eg: "*".txt``.
        ``CodeEg: GetAllFilesInDirectory(DIR=[YourPath], Extension=["*".txt])``
        """
        LISTOFFILES = []   
        os.chdir(DIR)
        for each_file in glob.glob(Extension):
            LISTOFFILES.append(each_file)
        return LISTOFFILES
DirHelper = DirectoryHepler()
jEdit = FCS_JsonSystem_Editor()

"""
Plugin GUI Enums ID's Tpyes
____________________________________________________________________
"""
class GlobalEnumsAndMainRegisterIDs(object):
    """
    Main C4D Plugin Tool Register ID's

    ``Eg.:``
    SNG_PLUGIN_ID = 0000000

    WARNING! Please obtain your own plugin ID 
    from http://www.plugincafe.com by Maxon and 
    Its free to get a ID.

    and

    GLOBAL STRINGS TEXT in the c4d_strings.str // 
    Calling on the IDs to get Text String

    GLOBAL TOOLS UI ID's in the c4d_symbols.h file

    ``Eg.:``

     In the c4d_symbols.h file :

     IDS_ID  = 1109

     In the c4d_strings.str file :

     IDS_ID   "HelloWorld";

    """
    SMP_PLUGIN_ID                   = 1050237

    IDS_SMP_NAME                    = 10000
    IDS_SMP_DESC                    = 10001

    ID_SMP_CENTER_AXIS 				= 10007
    ID_SMP_APPLY_COLOR 				= 10008
    ID_SMP_ATTACH_CHILD 			= 10009
    ID_SMP_REMOVE_ALL_MAT_TAGS		= 10011
    ID_SMP_OPTIMIZE_SPLITOBJ 		= 10012
    ID_SMP_OPTIMIZE_MAINOBJ 		= 10013
    ID_SMP_DELETE_POLY 				= 10014
    ID_SMP_SPLIT_WHOLE_OBJECT 		= 10002
    ID_SMP_REMOVE_UNUSED_MATS       = 10015
    ID_SMP_ABOUT                    = 10004
    ID_SMP_HELP                     = 10005
    ID_SMP_STAY_POLYMODE            = 10016
fcsID = GlobalEnumsAndMainRegisterIDs()
strId = fcsID







"""
Plugin Core
____________________________________________________________________
"""
# [  Plugin Options Menu ]
# [  User Global Settings Properties Data ]
ID_SMP_CENTER_AXIS 				= True
ID_SMP_APPLY_COLOR 				= True
ID_SMP_ATTACH_CHILD 			= True
ID_SMP_REMOVE_ALL_MAT_TAGS		= False
ID_SMP_OPTIMIZE_SPLITOBJ 		= True
ID_SMP_OPTIMIZE_MAINOBJ 		= False
ID_SMP_DELETE_POLY 				= False
ID_SMP_SPLIT_WHOLE_OBJECT 		= False
ID_SMP_REMOVE_UNUSED_MATS       = True
ID_SMP_STAY_POLYMODE            = False
RunDefaultOnce = True
# [  Declare menu items IDs Data ]
IDM_ITEM1 = c4d.FIRST_POPUP_ID
IDM_ITEM2 = {'id':c4d.FIRST_POPUP_ID+1, 'str':"", 'IV':ID_SMP_ATTACH_CHILD }
IDM_ITEM3 = {'id':c4d.FIRST_POPUP_ID+2, 'str':"", 'IV':ID_SMP_DELETE_POLY }
IDM_ITEM4 = {'id':c4d.FIRST_POPUP_ID+3, 'str':"", 'IV':ID_SMP_OPTIMIZE_SPLITOBJ }
IDM_ITEM5 = {'id':c4d.FIRST_POPUP_ID+4, 'str':"", 'IV':ID_SMP_OPTIMIZE_MAINOBJ }
IDM_ITEM6 = {'id':c4d.FIRST_POPUP_ID+5, 'str':"", 'IV':ID_SMP_CENTER_AXIS }
IDM_ITEM7 = {'id':c4d.FIRST_POPUP_ID+6, 'str':"", 'IV':ID_SMP_APPLY_COLOR }
IDM_ITEM8 = {'id':c4d.FIRST_POPUP_ID+7, 'str':"", 'IV':ID_SMP_REMOVE_ALL_MAT_TAGS }
IDM_ITEM9 = {'id':c4d.FIRST_POPUP_ID+8, 'str':"", 'IV':ID_SMP_SPLIT_WHOLE_OBJECT }
IDM_ITEM14 = {'id':c4d.FIRST_POPUP_ID+13, 'str':"", 'IV':ID_SMP_REMOVE_UNUSED_MATS}
IDM_ITEM15 = {'id':c4d.FIRST_POPUP_ID+14, 'str':"", 'IV':ID_SMP_STAY_POLYMODE}
IDM_ITEM10 = c4d.FIRST_POPUP_ID+9
IDM_ITEM11 = c4d.FIRST_POPUP_ID+10
IDM_ITEM12 = c4d.FIRST_POPUP_ID+11
IDM_ITEM13 = c4d.FIRST_POPUP_ID+12
IDM_ITEM16 = c4d.FIRST_POPUP_ID+15
IDM_ITEM17 = c4d.FIRST_POPUP_ID+16
# [ Web Link Helper ]
def OpenWebLinks(webLink):
    link = FCSWEBLINKS.get(webLink)
    if link:
        webbrowser.open(link, new=2, autoraise=True)
    return True
# [ Tool Save Config File  ]
class TrueSplitConfigTemplate(object):
    """ Prefs Tool Json ConfigFile """
    def __init__(self, ID_SMP_ATTACH_CHILD, ID_SMP_CENTER_AXIS, ID_SMP_APPLY_COLOR, ID_SMP_REMOVE_ALL_MAT_TAGS, ID_SMP_OPTIMIZE_SPLITOBJ, ID_SMP_OPTIMIZE_MAINOBJ, ID_SMP_DELETE_POLY, ID_SMP_REMOVE_UNUSED_MATS, ID_SMP_STAY_POLYMODE, ID_SMP_SPLIT_WHOLE_OBJECT):
        self.ID_SMP_ATTACH_CHILD = ID_SMP_ATTACH_CHILD
        self.ID_SMP_CENTER_AXIS = ID_SMP_CENTER_AXIS
        self.ID_SMP_APPLY_COLOR = ID_SMP_APPLY_COLOR
        self.ID_SMP_REMOVE_ALL_MAT_TAGS = ID_SMP_REMOVE_ALL_MAT_TAGS
        self.ID_SMP_OPTIMIZE_SPLITOBJ = ID_SMP_OPTIMIZE_SPLITOBJ
        self.ID_SMP_OPTIMIZE_MAINOBJ = ID_SMP_OPTIMIZE_MAINOBJ
        self.ID_SMP_DELETE_POLY = ID_SMP_DELETE_POLY
        self.ID_SMP_REMOVE_UNUSED_MATS = ID_SMP_REMOVE_UNUSED_MATS
        self.ID_SMP_STAY_POLYMODE = ID_SMP_STAY_POLYMODE
        self.ID_SMP_SPLIT_WHOLE_OBJECT = ID_SMP_SPLIT_WHOLE_OBJECT
def SaveUserTrueSplitData(plug_PATH):
    # [ Get Global Properties to Edit Them. ]
    global ID_SMP_CENTER_AXIS
    global ID_SMP_APPLY_COLOR
    global ID_SMP_ATTACH_CHILD
    global ID_SMP_REMOVE_ALL_MAT_TAGS
    global ID_SMP_OPTIMIZE_SPLITOBJ
    global ID_SMP_OPTIMIZE_MAINOBJ
    global ID_SMP_DELETE_POLY
    global ID_SMP_SPLIT_WHOLE_OBJECT
    global ID_SMP_REMOVE_UNUSED_MATS
    global ID_SMP_STAY_POLYMODE  

    data = TrueSplitConfigTemplate(ID_SMP_ATTACH_CHILD=jEdit.JsonBoolConverter(ID_SMP_ATTACH_CHILD), 
                                    ID_SMP_CENTER_AXIS=jEdit.JsonBoolConverter(ID_SMP_CENTER_AXIS),
                                    ID_SMP_APPLY_COLOR=jEdit.JsonBoolConverter(ID_SMP_APPLY_COLOR),
                                    ID_SMP_REMOVE_ALL_MAT_TAGS=jEdit.JsonBoolConverter(ID_SMP_REMOVE_UNUSED_MATS),
                                    ID_SMP_OPTIMIZE_SPLITOBJ=jEdit.JsonBoolConverter(ID_SMP_OPTIMIZE_SPLITOBJ),
                                    ID_SMP_OPTIMIZE_MAINOBJ=jEdit.JsonBoolConverter(ID_SMP_OPTIMIZE_MAINOBJ),
                                    ID_SMP_DELETE_POLY=jEdit.JsonBoolConverter(ID_SMP_DELETE_POLY),
                                    ID_SMP_REMOVE_UNUSED_MATS=jEdit.JsonBoolConverter(ID_SMP_REMOVE_UNUSED_MATS),
                                    ID_SMP_STAY_POLYMODE=jEdit.JsonBoolConverter(ID_SMP_STAY_POLYMODE),
                                    ID_SMP_SPLIT_WHOLE_OBJECT=jEdit.JsonBoolConverter(ID_SMP_SPLIT_WHOLE_OBJECT))

    jEdit.SaveFile(os.path.join(plug_PATH['PREFS'], "UserTrueSplitConfig.json"), data.__dict__ )
    return True
def LoadUserTrueSplitData(plug_PATH):
    # [ Get Global Properties to Edit Them. ]
    global ID_SMP_CENTER_AXIS
    global ID_SMP_APPLY_COLOR
    global ID_SMP_ATTACH_CHILD
    global ID_SMP_REMOVE_ALL_MAT_TAGS
    global ID_SMP_OPTIMIZE_SPLITOBJ
    global ID_SMP_OPTIMIZE_MAINOBJ
    global ID_SMP_DELETE_POLY
    global ID_SMP_SPLIT_WHOLE_OBJECT
    global ID_SMP_REMOVE_UNUSED_MATS
    global ID_SMP_STAY_POLYMODE  

    config = os.path.join(plug_PATH['PREFS'], "UserTrueSplitConfig.json")
    if os.path.exists(config):
        ID_SMP_ATTACH_CHILD = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_ATTACH_CHILD"))
        IDM_ITEM2['IV'] = ID_SMP_ATTACH_CHILD

        ID_SMP_CENTER_AXIS = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_CENTER_AXIS"))
        IDM_ITEM6['IV'] = ID_SMP_CENTER_AXIS

        ID_SMP_APPLY_COLOR = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_APPLY_COLOR"))
        IDM_ITEM7['IV'] = ID_SMP_APPLY_COLOR

        ID_SMP_REMOVE_ALL_MAT_TAGS = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_REMOVE_ALL_MAT_TAGS"))
        IDM_ITEM8['IV'] = ID_SMP_REMOVE_ALL_MAT_TAGS

        ID_SMP_OPTIMIZE_SPLITOBJ = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_OPTIMIZE_SPLITOBJ"))
        IDM_ITEM4['IV'] = ID_SMP_OPTIMIZE_SPLITOBJ

        ID_SMP_OPTIMIZE_MAINOBJ = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_OPTIMIZE_MAINOBJ"))
        IDM_ITEM5['IV'] = ID_SMP_OPTIMIZE_MAINOBJ

        ID_SMP_DELETE_POLY = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_DELETE_POLY"))
        IDM_ITEM3['IV'] = ID_SMP_DELETE_POLY

        ID_SMP_SPLIT_WHOLE_OBJECT = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_SPLIT_WHOLE_OBJECT"))
        IDM_ITEM9['IV'] = ID_SMP_SPLIT_WHOLE_OBJECT

        ID_SMP_REMOVE_UNUSED_MATS = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_REMOVE_UNUSED_MATS"))
        IDM_ITEM14['IV'] = ID_SMP_REMOVE_UNUSED_MATS

        ID_SMP_STAY_POLYMODE = jEdit.BoolCheckerHelper(jEdit.GetItemProperty(config, "ID_SMP_STAY_POLYMODE"))
        IDM_ITEM15['IV'] = ID_SMP_STAY_POLYMODE    
    else:
        pass
    return True
# [ Reset To Default Settings  ]
def ResetSettings():
    # [ Get Global Properties to Edit Them. ]
    global ID_SMP_CENTER_AXIS
    global ID_SMP_APPLY_COLOR
    global ID_SMP_ATTACH_CHILD
    global ID_SMP_REMOVE_ALL_MAT_TAGS
    global ID_SMP_OPTIMIZE_SPLITOBJ
    global ID_SMP_OPTIMIZE_MAINOBJ
    global ID_SMP_DELETE_POLY
    global ID_SMP_SPLIT_WHOLE_OBJECT
    global ID_SMP_REMOVE_UNUSED_MATS
    global ID_SMP_STAY_POLYMODE      

    # [ Set Global Properties to Edit Them. ]
    ID_SMP_CENTER_AXIS 				= True
    ID_SMP_APPLY_COLOR 				= True
    ID_SMP_ATTACH_CHILD 			= True
    ID_SMP_REMOVE_ALL_MAT_TAGS		= False
    ID_SMP_OPTIMIZE_SPLITOBJ 		= True
    ID_SMP_OPTIMIZE_MAINOBJ 		= False
    ID_SMP_DELETE_POLY 				= False
    ID_SMP_SPLIT_WHOLE_OBJECT 		= False
    ID_SMP_REMOVE_UNUSED_MATS       = True
    ID_SMP_STAY_POLYMODE            = False

    IDM_ITEM2['IV'] = ID_SMP_ATTACH_CHILD
    IDM_ITEM6['IV'] = ID_SMP_CENTER_AXIS
    IDM_ITEM7['IV'] = ID_SMP_APPLY_COLOR
    IDM_ITEM8['IV'] = ID_SMP_REMOVE_ALL_MAT_TAGS
    IDM_ITEM4['IV'] = ID_SMP_OPTIMIZE_SPLITOBJ
    IDM_ITEM5['IV'] = ID_SMP_OPTIMIZE_MAINOBJ
    IDM_ITEM3['IV'] = ID_SMP_DELETE_POLY
    IDM_ITEM9['IV'] = ID_SMP_SPLIT_WHOLE_OBJECT
    IDM_ITEM14['IV'] = ID_SMP_REMOVE_UNUSED_MATS
    IDM_ITEM15['IV'] = ID_SMP_STAY_POLYMODE   
    return True
# [ Menu GUI ]
def MenuPopupUI(global_strings, plug_PATH):
    """ User Options Menu  """

    global RunDefaultOnce

    # [ Get Global Properties to Edit Them. ]
    global ID_SMP_CENTER_AXIS
    global ID_SMP_APPLY_COLOR
    global ID_SMP_ATTACH_CHILD
    global ID_SMP_REMOVE_ALL_MAT_TAGS
    global ID_SMP_OPTIMIZE_SPLITOBJ
    global ID_SMP_OPTIMIZE_MAINOBJ
    global ID_SMP_DELETE_POLY
    global ID_SMP_SPLIT_WHOLE_OBJECT
    global ID_SMP_REMOVE_UNUSED_MATS
    global ID_SMP_STAY_POLYMODE   

    # [ Set global_strings menu item Properties ]
    IDS = global_strings

    if RunDefaultOnce == True:
        IDM_ITEM2['str'] = IDS.ID(strId.ID_SMP_ATTACH_CHILD)
        IDM_ITEM6['str'] = IDS.ID(strId.ID_SMP_CENTER_AXIS)
        IDM_ITEM7['str'] = IDS.ID(strId.ID_SMP_APPLY_COLOR)
        IDM_ITEM8['str'] = IDS.ID(strId.ID_SMP_REMOVE_ALL_MAT_TAGS)
        IDM_ITEM4['str'] = IDS.ID(strId.ID_SMP_OPTIMIZE_SPLITOBJ)
        IDM_ITEM5['str'] = IDS.ID(strId.ID_SMP_OPTIMIZE_MAINOBJ)
        IDM_ITEM3['str'] = IDS.ID(strId.ID_SMP_DELETE_POLY)
        IDM_ITEM9['str'] = IDS.ID(strId.ID_SMP_SPLIT_WHOLE_OBJECT)
        IDM_ITEM14['str'] = IDS.ID(strId.ID_SMP_REMOVE_UNUSED_MATS)
        IDM_ITEM15['str'] = IDS.ID(strId.ID_SMP_STAY_POLYMODE)



    # [ C4D Menu Settings]
    DisableMenuItem = '&d&'  # Disable this item
    CheckMenuItem   = '&c&'  # Checkbox this item


    def AddItemChecked(item):
        if item['IV'] == True:
            menu.InsData(item['id'], str(item['str'])+CheckMenuItem)
        else:
            menu.InsData(item['id'], str(item['str']))
        return True

    def IfSettingsChecked(property_set):
        """
        Create Main menu 
        ______________________________________________
        """
        #print(property_set)
        state = None
        if property_set == True:
            state = False
        else:
            state = True
        return state


    """
    Create Main menu 
    ______________________________________________
    """
    menu = c4d.BaseContainer()
    menu.InsData(0, '') # Append separator    
    AddItemChecked(item=IDM_ITEM2)
    AddItemChecked(item=IDM_ITEM3)
    menu.InsData(0, '') # Append separator    
    AddItemChecked(item=IDM_ITEM4)
    AddItemChecked(item=IDM_ITEM5)
    AddItemChecked(item=IDM_ITEM6)
    menu.InsData(0, '') # Append separator    
    AddItemChecked(item=IDM_ITEM7)
    AddItemChecked(item=IDM_ITEM14)
    AddItemChecked(item=IDM_ITEM8)
    AddItemChecked(item=IDM_ITEM15)
    menu.InsData(0, '') # Append separator   
    AddItemChecked(item=IDM_ITEM9)
    menu.InsData(0, '') # Append separator   
    menu.InsData(IDM_ITEM17, "&i12373&Reset Settings to Default")
    menu.InsData(0, '') # Append separator 
    # [ Help Menu - A Sub Menu ]
    menuHelp = c4d.BaseContainer()
    menuHelp.InsData( 1, "&i12373&"+IDS.ID(strId.ID_SMP_HELP) )
    #menuHelp.InsData(IDM_ITEM10, "&i12373&Online Manual")
    menuHelp.InsData(IDM_ITEM16, "&i12373&Documentation")
    menuHelp.InsData(IDM_ITEM11, "&i12373&Gumroad Store")
    menuHelp.InsData(IDM_ITEM12, "&i12373&Discord Support")
    menu.SetContainer(IDM_ITEM13, menuHelp) # Set submenu as subcontainer
    menu.InsData( IDM_ITEM1, "&i12373&"+IDS.ID(strId.ID_SMP_ABOUT) )


    """
    Commands of Main menu 
    ______________________________________________
    """
    result = c4d.gui.ShowPopupDialog(cd=None, bc=menu, x=c4d.MOUSEPOS, y=c4d.MOUSEPOS)

    if result == IDM_ITEM1:
        gui.MessageDialog(AboutTool)

    if result == IDM_ITEM17:
        ResetSettings()
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM16:
        RunToolWin = Maunal(path=plug_PATH)
        RunToolWin.OpenToolWindow()        

    if result == IDM_ITEM11:
        OpenWebLinks(webLink="GUM")

    if result == IDM_ITEM12:
        OpenWebLinks(webLink="DIS")

    if result == IDM_ITEM2['id']:
        ID_SMP_ATTACH_CHILD = IfSettingsChecked(property_set=ID_SMP_ATTACH_CHILD)
        IDM_ITEM2['IV'] = ID_SMP_ATTACH_CHILD
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM3['id']:
        ID_SMP_DELETE_POLY = IfSettingsChecked(property_set=ID_SMP_DELETE_POLY)
        IDM_ITEM3['IV'] = ID_SMP_DELETE_POLY
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM4['id']:
        ID_SMP_OPTIMIZE_SPLITOBJ = IfSettingsChecked(property_set=ID_SMP_OPTIMIZE_SPLITOBJ)
        IDM_ITEM4['IV'] = ID_SMP_OPTIMIZE_SPLITOBJ
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM5['id']:
        ID_SMP_OPTIMIZE_MAINOBJ = IfSettingsChecked(property_set=ID_SMP_OPTIMIZE_MAINOBJ)
        IDM_ITEM5['IV'] = ID_SMP_OPTIMIZE_MAINOBJ
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM6['id']:
        ID_SMP_CENTER_AXIS = IfSettingsChecked(property_set=ID_SMP_CENTER_AXIS)
        IDM_ITEM6['IV'] = ID_SMP_CENTER_AXIS
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM7['id']:
        ID_SMP_APPLY_COLOR = IfSettingsChecked(property_set=ID_SMP_APPLY_COLOR)
        IDM_ITEM7['IV'] = ID_SMP_APPLY_COLOR
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM8['id']:
        ID_SMP_REMOVE_ALL_MAT_TAGS = IfSettingsChecked(property_set=ID_SMP_REMOVE_ALL_MAT_TAGS)
        IDM_ITEM8['IV'] = ID_SMP_REMOVE_ALL_MAT_TAGS
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM14['id']:
        ID_SMP_REMOVE_UNUSED_MATS = IfSettingsChecked(property_set=ID_SMP_REMOVE_UNUSED_MATS)
        IDM_ITEM14['IV'] = ID_SMP_REMOVE_UNUSED_MATS
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM15['id']:
        ID_SMP_STAY_POLYMODE = IfSettingsChecked(property_set=ID_SMP_STAY_POLYMODE)
        IDM_ITEM15['IV'] = ID_SMP_STAY_POLYMODE
        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    if result == IDM_ITEM9['id']:
        RunDefaultOnce = False
        ID_SMP_SPLIT_WHOLE_OBJECT = IfSettingsChecked(property_set=ID_SMP_SPLIT_WHOLE_OBJECT)
        IDM_ITEM9['IV'] = ID_SMP_SPLIT_WHOLE_OBJECT
        DisableItems = [ IDM_ITEM2, IDM_ITEM3, IDM_ITEM4, IDM_ITEM5, IDM_ITEM8, IDM_ITEM13, IDM_ITEM15 ]
        if ID_SMP_SPLIT_WHOLE_OBJECT == True:
            for menuItem in DisableItems:
                print(menuItem['str']+DisableMenuItem)
                menuItem['str'] = menuItem['str']+DisableMenuItem
        else:
            for menuItem in DisableItems:
                print(menuItem['str'].split(DisableMenuItem)[0])
                menuItem['str'] = menuItem['str'].split(DisableMenuItem)[0]            

        SaveUserTrueSplitData(plug_PATH=plug_PATH)

    return True
# ===========================================================================#
# [ Main Core Functions ]
MATTEXTAG = c4d.Ttexture    # Material Texure Tag / Look at TagTypes in C4D Python SDK
SELECTIONTAG = c4d.Tpolygonselection
def GetSelectionOfSplitObjs(doc, list_objs):
    for obj in list_objs:
        #doc.SetActiveObject(obj)
        doc.SetSelection(obj, c4d.SELECTION_ADD)
    c4d.EventAdd()
    return True
def GetC4DObjectData(obj):
    """ Get C4D Object Data  """
    
    # Get Object Id ( A int ).
    Id = obj.GetGUID()
    
    # Get Object Name ( A String ).
    Name = obj.GetName()
    
    # Get All Object Tags.
    Tags = obj.GetTags()
    
    # Get Object and Check if Object is a Polygon ( An Editable Mesh ).
    if obj.CheckType(c4d.Opolygon):
        # Get Object polygons amounts. ( A int ).
        PolygonAmount = obj.GetPolygonCount()
        # Get the selected polygon which is BaseSelect( bs ).
        PolygonSelect = obj.GetPolygonS()
    else:
        PolygonAmount = None
        PolygonSelect = None
        pass
        
    return Id, Name, Tags, PolygonAmount, PolygonSelect
def SplitPolyOperationCore(doc, obj, tag):
    doc.StartUndo()

    Oid, Oname, Otags, OpolyAmount, OpolySel = GetC4DObjectData(obj=obj)

    doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)
    c4d.CallCommand(12187) # Enable Polygons Mode
    if tag:
        c4d.CallButton(tag, c4d.POLYGONSELECTIONTAG_COMMAND1)

    #for index, selected in enumerate(OpolySel.GetAll(OpolyAmount)):
    #    if not selected: 
    #        gui.MessageDialog("No Polygon Selection. Please select a polygon face on the object to split.")                                   
    #        return
    #    else:

    settings = c4d.BaseContainer()
    res = c4d.utils.SendModelingCommand(c4d.MCOMMAND_SPLIT,
                list = [obj],
                mode = c4d.MODELINGCOMMANDMODE_POLYGONSELECTION,
                bc = settings,
                doc = doc)           
    res[0].InsertAfter(obj)
    if tag:
        c4d.CallButton(tag, c4d.POLYGONSELECTIONTAG_COMMAND4)
    splitObj = res[0]
    doc.AddUndo(c4d.UNDOTYPE_NEW, splitObj)

    doc.EndUndo()
    return splitObj
def RemoveObjectChildren(doc, parent_object):
    """ 
    Remove Childen from Split Object.

    Function Return : Selection of the ``parent_object``
    """
    doc.SetActiveObject(parent_object)
    c4d.CallCommand(100004768) # Select Children
    doc.SetSelection(parent_object, c4d.SELECTION_SUB)
    SplitObjChildren = doc.GetActiveObjects(1)
    if SplitObjChildren:
        for child_obj in SplitObjChildren:
            print(child_obj)
            child_obj.Remove()
    else: 
        pass
    return doc.SetActiveObject(parent_object)
def RemoveUnusedMateralsDeeperChecked(doc, split_Object):
    """
    Remove Unused Materals Tags Double Tap Operation
    """
    Oid, Oname, Otags, OpolyAmount, OpolySel = GetC4DObjectData(obj=split_Object)
    Otags.reverse()
    orgName = Oname

    for run_ops in range(3):
        for eachTag in Otags:
            if eachTag.CheckType(MATTEXTAG):
                texTagSelectionName = eachTag[c4d.TEXTURETAG_RESTRICTION]
                matName = eachTag[c4d.TEXTURETAG_MATERIAL]
                if texTagSelectionName:
                    Otags.reverse()
                    for eachSelTag in Otags:
                        if eachSelTag.CheckType(SELECTIONTAG):
                            if eachSelTag.GetName() == texTagSelectionName:
                                c4d.CallButton(eachSelTag, c4d.POLYGONSELECTIONTAG_COMMAND1)
                                listy=[]
                                for index, selected in enumerate(OpolySel.GetAll(OpolyAmount)):
                                    if not selected:
                                        continue
                                    else:
                                        listy.append(index)

                                if len(listy) == 0:
                                    eachTag.Remove()
                                    eachSelTag.Remove()
                else:
                    eachTag.Remove()
                    
    # Detect Right Material for the Polygon collection
    eachMatTag = Otags[0]
    if eachMatTag.CheckType(MATTEXTAG):
        texTagSelectionName = eachMatTag[c4d.TEXTURETAG_RESTRICTION]
        matName = eachMatTag[c4d.TEXTURETAG_MATERIAL]

        print(str(texTagSelectionName) + " : " + str(matName))

        for eachTag in Otags:
            if eachTag.CheckType(MATTEXTAG):
                if eachTag[c4d.TEXTURETAG_MATERIAL] == matName:
                    pass
                else:
                    eachTag.Remove()
    for eachTag in Otags:
        if eachTag.CheckType(MATTEXTAG):
            if eachTag[c4d.TEXTURETAG_MATERIAL] == eachMatTag[c4d.TEXTURETAG_MATERIAL]:
                pass
            else:
                texTagSelectionName = eachTag[c4d.TEXTURETAG_RESTRICTION]
                matName = eachTag[c4d.TEXTURETAG_MATERIAL]
                if texTagSelectionName:
                    Otags.reverse()
                    for eachSelTag in Otags:
                        if eachSelTag.CheckType(SELECTIONTAG):
                            if eachSelTag.GetName() == texTagSelectionName:
                                c4d.CallButton(eachSelTag, c4d.POLYGONSELECTIONTAG_COMMAND1)
                                listy=[]
                                for index, selected in enumerate(OpolySel.GetAll(OpolyAmount)):
                                    if not selected:
                                        continue
                                    else:
                                        listy.append(index)
                                if len(listy) == 0:
                                    eachTag.Remove()
                                    eachSelTag.Remove()
                else:
                    eachTag.Remove()
            for eachSelTag in Otags:
                if eachSelTag.CheckType(SELECTIONTAG):
                    if eachTag[c4d.TEXTURETAG_RESTRICTION] == eachSelTag.GetName():
                        pass
                    else:
                        eachTag.Remove()
        else:
            pass



    return True
def RemoveUnusedMateralsTags(doc, split_Object):
    """ 
    Remove Unused Materals Tags Double Tap Operation 
    """
    Oid, Oname, Otags, OpolyAmount, OpolySel = GetC4DObjectData(obj=split_Object)
    Otags.reverse()
    orgName = Oname
    for run_ops in range(5):
        for eachTag in Otags:
            if eachTag.CheckType(MATTEXTAG):
                texTagSelectionName = eachTag[c4d.TEXTURETAG_RESTRICTION]
                Otags.reverse()
                for eachSelTag in Otags:
                    if eachSelTag.CheckType(SELECTIONTAG):
                        if eachSelTag.GetName() == texTagSelectionName:
                            c4d.CallButton(eachSelTag, c4d.POLYGONSELECTIONTAG_COMMAND1)
                            listy=[]
                            for index, selected in enumerate(OpolySel.GetAll(OpolyAmount)):
                                if not selected:                                    
                                    continue
                                else:
                                    listy.append(index)
                            if len(listy) == 0:
                                eachTag.Remove()
                                eachSelTag.Remove()

    for run_ops2 in range(5):
        RemoveUnusedMateralsDeeperChecked(doc=doc, split_Object=split_Object)

    return True
def RemoveAllMateralsTags(doc, split_Object):
    """ 
    Remove Unused Materals Tags Double Tap Operation 
    """
    Oid, Oname, Otags, OpolyAmount, OpolySel = GetC4DObjectData(obj=split_Object)
    Otags.reverse()
    orgName = Oname
    for run_ops in range(3):
        for eachTag in Otags:
            if eachTag.CheckType(MATTEXTAG):
                texTagSelectionName = eachTag[c4d.TEXTURETAG_RESTRICTION]
                Otags.reverse()
                for eachSelTag in Otags:
                    if eachSelTag.CheckType(SELECTIONTAG):
                        #print("needed to go")
                        eachTag.Remove()
                        eachSelTag.Remove()
    # Set Back to the Org obj name.
    #split_Object.SetName(orgName)
    #doc.SetSelection(split_Object, c4d.SELECTION_ADD)  
    return True
def DeletePolygon(doc, base_object, split_Object):
    """ 
    Delete Polygon After Split 

    Function Return : Selection of the ``split_Object``
    """
    settings = c4d.BaseContainer()
    res2 = c4d.utils.SendModelingCommand(c4d.MCOMMAND_DELETE,
                list = [base_object],
                mode = c4d.MODELINGCOMMANDMODE_POLYGONSELECTION,
                bc = settings,
                doc = doc)
    return doc.SetActiveObject(split_Object)
def ApplyColor(doc, obj, color):
    """ 
    Apply Color To the Split Object Operation 
    """
    obj[c4d.ID_BASEOBJECT_USECOLOR]=1
    obj[c4d.ID_BASEOBJECT_COLOR]=color
    doc.AddUndo(c4d.UNDOTYPE_NEW, obj)
    return True
def OptimizePolygon(doc, poly_object):
    """ 
    Optimize Object
    """
    doc.SetActiveObject(poly_object)
    doc.SetSelection(poly_object, c4d.SELECTION_ADD)  
    c4d.CallCommand(12139) # Points
    c4d.CallCommand(13323) # Select All
    c4d.CallCommand(14039) # Optimize...
    c4d.CallCommand(12187) # Enable Polygons Mode
    return True
def CenterAxisSplitPolygon(doc, poly_object):
    """ 
    Center Axis Polygon After Split 

    Function Return : Selection of the ``split_Object``
    """
    doc.SetActiveObject(poly_object)
    #c4d.CallCommand(1019940) # Reset PSR
    c4d.CallCommand(1011982) # Center Axis
    return doc.SetActiveObject(poly_object)
def SplitsWholeEntireObject(doc, obj, cetnerAxis, applyCol):
    """ Convert To Direct X GeoFormat """

    processWin = ProcessPopupWindow(" TrueSplit : Calulating Object please wait... | Running")
    processWin.openWindowUI(c4d.DLG_TYPE_ASYNC) 

    doc.StartUndo()

    doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)

    # Get Object and make a clone of the object
    CloneTemp = obj.GetClone(c4d.COPYFLAGS_0)
    doc.InsertObject(CloneTemp)
    doc.AddUndo(c4d.UNDOTYPE_NEW, CloneTemp)

    c4d.CallCommand(13323, 13323) # Select All

    settings = c4d.BaseContainer()
    settings.SetData(c4d.MDATA_DISCONNECT_PRESERVEGROUPS, False)
    res = c4d.utils.SendModelingCommand(c4d.MCOMMAND_DISCONNECT,
                list = [CloneTemp],
                mode = c4d.MODELINGCOMMANDMODE_POLYGONSELECTION,
                bc = settings,
                doc = doc)
    doc.SetActiveObject(CloneTemp)

    c4d.CallCommand(17891) # Polygon Groups to Objects

    doc.SetActiveObject(CloneTemp)

    c4d.CallCommand(100004768) # Select Children

    doc.SetSelection(obj, c4d.SELECTION_SUB)
    SplitObjChildren = doc.GetActiveObjects(1)
    currentNum = 1
    for child_obj in SplitObjChildren:

        processWin.processBAR.Run_ProgressBar(1001, 1010, currentNum, len(SplitObjChildren), c4d.Vector(0, 0.78125, 0.99609375))

        RemoveUnusedMateralsTags(doc=doc, split_Object=child_obj)

        if cetnerAxis == True:
            CenterAxisSplitPolygon(doc=doc, poly_object=child_obj)

        if applyCol == True:
            ApplyColor(doc=doc, obj=child_obj, color=AddVectorRandomColors())

        child_obj.InsertUnder(obj) 

        doc.AddUndo(c4d.UNDOTYPE_NEW, child_obj)

        currentNum += 1

    CloneTemp.Remove()

    doc.AddUndo(c4d.UNDOTYPE_NEW, CloneTemp)

    doc.SetActiveObject(obj)

    doc.EndUndo()

    c4d.EventAdd()
    c4d.StatusClear()

    processWin.processBAR.Stop_ProgressBar(1010)
    processWin.closeWindowUI()

    c4d.EventAdd()
    return True

    # [ To add and create random colors to UI elements and objects.]
def AddVectorRandomColors():
    """
    To add and create random colors to UI elements and objects.
    """
    r = randint(0,255) / 256.0
    g = randint(0,255) / 256.0
    b = randint(0,255) / 256.0
    return c4d.Vector(r, g, b)
# [ Tool Core Functions ]
SPLIT_OBJS = []
def SplitMyPoly(doc, obj):

    doc.StartUndo()

    doc.AddUndo(c4d.UNDOTYPE_CHANGE, obj)

    if ID_SMP_SPLIT_WHOLE_OBJECT == False:

        # Split Object
        SplitObj = SplitPolyOperationCore(doc=doc, obj=obj, tag=None)
        # [ Set Split Object Name ]
        SplitObj.SetName(obj.GetName()+"_split")
        # [ Default Core ]
        RemoveObjectChildren(doc=doc, parent_object=SplitObj)
        if ID_SMP_REMOVE_UNUSED_MATS == True:
            RemoveUnusedMateralsTags(doc=doc, split_Object=SplitObj)

        c4d.CallCommand(100004767) # Deselect All

        if ID_SMP_DELETE_POLY == True:
            DeletePolygon(doc=doc, base_object=obj, split_Object=SplitObj)

        # Optimize Split Object and Main Ojbect
        if ID_SMP_OPTIMIZE_SPLITOBJ == True:
            OptimizePolygon(doc=doc, poly_object=SplitObj)
        if ID_SMP_OPTIMIZE_MAINOBJ == True:    
            OptimizePolygon(doc=doc, poly_object=obj)

        # Add Color To Split Object
        if ID_SMP_APPLY_COLOR == True:
            ApplyColor(doc=doc, obj=SplitObj, color=AddVectorRandomColors())

        # Remove All Materials Tags on Split Object
        if ID_SMP_REMOVE_ALL_MAT_TAGS == True:
            RemoveAllMateralsTags(doc=doc, split_Object=SplitObj)

        # Split Object as a Child of Main Object
        if ID_SMP_ATTACH_CHILD == True:
            SplitObj.InsertUnder(obj)
            doc.SetActiveObject(SplitObj)
            c4d.CallCommand(1019940) # Reset PSR           

        if ID_SMP_ATTACH_CHILD == False:
            SplitObj.InsertUnder(obj)
            doc.SetActiveObject(SplitObj)
            c4d.CallCommand(1019940) # Reset PSR
            UnGrpObjs = []
            doc.SetActiveObject(obj)
            c4d.CallCommand(100004768) # Select Children
            doc.SetSelection(obj, c4d.SELECTION_SUB)
            GetUnGrpObjs = doc.GetActiveObjects(1)
            for UnGObj in GetUnGrpObjs:
                UnGrpObjs.append(UnGObj)
            doc.SetActiveObject(obj)
            c4d.CallCommand(100004773) # Ungroup Objects
            for UnObj in UnGrpObjs:
                if UnObj.GetGUID() == SplitObj.GetGUID():
                    pass
                else:
                    UnObj.InsertUnder(obj)
            
        # Center Axis on the Split Object
        if ID_SMP_CENTER_AXIS == True:
            CenterAxisSplitPolygon(doc=doc, poly_object=SplitObj)

        if ID_SMP_CENTER_AXIS == False:
            if ID_SMP_ATTACH_CHILD == True:
                SplitObj.InsertUnder(obj)

        SPLIT_OBJS.append(SplitObj)

    else:
        SplitsWholeEntireObject(doc=doc, obj=obj, cetnerAxis=ID_SMP_CENTER_AXIS, applyCol=ID_SMP_APPLY_COLOR)

    doc.EndUndo()
    c4d.EventAdd()    
    return True
# [ Plugin Command Data ]
class CMD(c4d.plugins.CommandData):
    """
    `` An C4D Command Data Plugin Class ``
    _________________________________________________
    """ 

    def __init__(self, global_strings, plug_PATH):
        super(CMD, self).__init__()
        self.IDS = global_strings

        global PLUGIN_PATH
        PLUGIN_PATH = plug_PATH
 
    """
    Main C4D GUI Dialog Operations Functions which 
    are Methods to Override for CommandData class.
    _________________________________________________
    """
    def Init(self, op):
        return True

    def Message(self, type, data):
        return True

    def Execute(self, doc):

        doc = c4d.documents.GetActiveDocument()

        SEL_C4D_OBJS = doc.GetActiveObjects(1)
        if not SEL_C4D_OBJS:
            gui.MessageDialog("A TrueSplit Error:\nIn order to use TrueSplit, you will need to select an objects.")
            return

        for sel_obj in SEL_C4D_OBJS:

            if sel_obj.CheckType(c4d.Opolygon):

                if doc.GetMode() == c4d.Mpolygons:
                        SplitMyPoly(doc=doc, obj=sel_obj)
                else:
                    gui.MessageDialog("A TrueSplit Error:\nIn order to useTrueSplit, you will need to be in polygon mode.")
                    print("A TrueSplit Error:\nIn order to use Split-My-Poly, you will need to be in polygon mode.")
                    return
            else:
                gui.MessageDialog("Object is not editable object. Please make object editable.")
                print("Object is not editable object. Please make object editable.")

        if ID_SMP_SPLIT_WHOLE_OBJECT == False:
            global SPLIT_OBJS

            c4d.CallCommand(12187) # Enable Polygons Mode

            GetSelectionOfSplitObjs(doc=doc, list_objs=SPLIT_OBJS)

            # Split Object as a Child of Main Object
            if ID_SMP_STAY_POLYMODE == True:
                c4d.CallCommand(12187) # Polygons
            
            if ID_SMP_STAY_POLYMODE == False:
                c4d.CallCommand(12298) # Model      

            SPLIT_OBJS = []


        c4d.EventAdd()
        return True
    
    def ExecuteOptionID(self, doc, plugid, subid):
        """
        This Method is for when you have a Gear Option 
        Settings Icon Button.
        _________________________________________________
        """ 
        MenuPopupUI(self.IDS, PLUGIN_PATH)
        return True
# [ Process Running Window ]
class ProcessPopupWindow(BaseWindowDialogUI):
    """ Process Running Window """
    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "TrueSplit Process Window"
    windowDialogWidthSize = 400
    windowDialogHeightSize = 10
    windowDialogPluginID = 0
    """
    Variables & Properties
    _________________________________________________
    """
    IDUI_BAR = 1010
    IDUI_STR = 1001

    def __init__(self, name):
        """
        The __init__ is an Constuctor and help get or set data
        into the class and passes data on from the another class.
        """
        super(ProcessPopupWindow, self).__init__()
        self.processBAR = AddCustomProgressBar_GUI(self)
        self.toolNameProcess = name
    
    def BuildUI(self):
        self.windowMainTileName = self.toolNameProcess
        self.processBAR.GUI( self.IDUI_STR, self.IDUI_BAR, 500, 15)
        return super(ProcessPopupWindow, self).BuildUI()
# [ Plugin Maunal Documentation GUI Window. ]
def TreeViewListUpdater(ui_widget, ItemsData, GrpId):
    dlg = ui_widget
    dlg.LayoutFlushGroup(GrpId)
    for grp_cat in ItemsData:
        dlg.TileBar.Add_BarTitle_GUI(bar_id=0, bar_name=grp_cat['grpCategoryName'], width=290, height=15, ui_color=BG_LitDarkBlue_Col)
        for page in grp_cat['grpListItems']:
            AddCustomNodeTextButton(dlg, page['btnId'], page['str'], 300, 10, page['state'])
    dlg.LayoutChanged(GrpId)
    dlg.LayoutChanged(1008)
    dlg.LayoutChanged(10042)
    return True
class Maunal(BaseWindowDialogUI):

    """TrueSplit Documentation Win"""

    """
    Window Dialog Main Properties
    _________________________________________________
    """
    windowMainTileName = "TrueSplit Documentation"
    windowDialogWidthSize = 850
    windowDialogHeightSize = 600
    windowDialogPluginID = 0
    f_doc_pages = []
    PagesSelected = []

    def OpenToolWindow(self):
        self.Open(c4d.DLG_TYPE_ASYNC, pluginid=strId.SMP_PLUGIN_ID, xpos=-2, ypos=-2, defaultw=850, defaulth=600, subid=0)
        return True

    def __init__(self, path):
        super(Maunal, self).__init__()
        self.docPath = path['LIB']
        self.doc_pages = []
        self.f_doc_pages = []
        self.PagesSelected = []  
        self._icon_buttons = []  



    def BuildUI(self):

 

        ui = self

        plugin_prefs_folder = os.path.join(self.docPath, 'Resources', 'manual_docs')

        self.GroupBegin(1008, c4d.BFH_SCALEFIT|c4d.BFV_SCALEFIT, 3, 0, "")
        self.GroupBorderNoTitle(c4d.BORDER_GROUP_IN)

        self.GroupBegin(1009, c4d.BFH_MASK|c4d.BFV_TOP, 1, 0, "  Table of Contents  ")
        self.GroupBorder(c4d.BORDER_GROUP_IN)

        os.chdir(os.path.join(plugin_prefs_folder, 'TrueSplitDoc'))
        for each_file in glob.glob("*"):
            doc_pages = []
            categoryName = " "+ each_file.split("_")[1] # 
            self.TileBar.Add_BarTitle_GUI(bar_id=0, bar_name=categoryName, width=290, height=15, ui_color=BG_LitDarkBlue_Col)
            for file in os.listdir(each_file):
                if file.endswith(".html"):
                    for files in file.splitlines():
                        fileName = files.split(".html")[0].split("_")[1]
                        ui_widget_id = GuiIdGenerator(IDsList=200000)
                        AddCustomNodeTextButton(self, ui_widget_id, " "+fileName, 300, 10, False)
                        pageItemData = {'btnId':ui_widget_id, 'dir':os.path.join(plugin_prefs_folder, 'TrueSplitDoc', each_file), 'htmlpages':file, 'str':" "+fileName, 'state':False}
                        doc_pages.append(pageItemData) 
                        self.PagesSelected.append(pageItemData)
            self.f_doc_pages.append( {'dir':os.path.join(plugin_prefs_folder, 'TrueSplitDoc', each_file), 'grpCategoryName':categoryName, 'grpListItems':doc_pages} ) 
 
        self.GroupEnd()

        self.AddSeparatorV(0, flags=c4d.BFV_SCALEFIT)

        self.HtmlWin.GUI(510, os.path.join(plugin_prefs_folder, 'TrueSplitDoc', '01_Introduction', "00_Welcome.html"))

        self.GroupEnd()
        return True

    def UIsettings(self):
        return True

    def DestroyWindow(self):
        self.doc_pages = []
        self.f_doc_pages = []
        self.PagesSelected = []  
        self._icon_buttons = []      
        global uiIDS
        uiIDS = 0     
        pass

    def UIfunctions(self, id):
        for p in self.PagesSelected:
            if id == p['btnId']:
                #print(p['str'])
                p['state'] = True
                self.HtmlWin.HtmlViewerUpdater( ui_widget_id=510, page_path=os.path.join(p['dir'], p['htmlpages']) )

                for unSelPage in self.PagesSelected:
                    if unSelPage['btnId'] == p['btnId']:
                        pass
                    else:
                        unSelPage['state'] = False

                self._icon_buttons = []
                return TreeViewListUpdater(ui_widget=self, ItemsData=self.f_doc_pages, GrpId=1009)
                
        return True










"""
Plugin Registration
____________________________________________________________________
"""
# [ Plugin Ids, 
#   WARNING! Please obtain your own plugin ID 
#   from http://www.plugincafe.com by Maxon and Its free to get a ID. ]
PLUGIN_ID = fcsID.SMP_PLUGIN_ID


# [ Plugin Directory Folder & Folders Paths ]
PLUGIN_PATH = { 
                "RES":os.path.join(os.path.dirname(__file__), 'res'), 
                "LIB":os.path.join(os.path.dirname(__file__), 'fcslib'), 
                "APPDATA":c4d.storage.GeGetStartupWritePath(),             # To find Cinema 4D Appdata Dir's Folder.
                "APP":c4d.storage.GeGetC4DPath(c4d.C4D_PATH_APPLICATION),  # The complete path to the host application. eg( (Mac) e.g: '/Applications/MAXON/Cinema 4D R12/Cinema 4D.app' )
                "PREFS":c4d.storage.GeGetC4DPath(c4d.C4D_PATH_PREFS)       # To find Cinema 4D Appdata Dir's of the Preferences Folder.
              }

# [ Plugin Register C4D Strings Text / Inside global string resource file, (res/strings_us/c4d_strings.str) and IDs are in (res/c4d_symbols.h) ]
class Get_Global_Plugin_Strings(object):
    """
    Get Plugin Global Strings Text from the c4d_strings.str
    Calling on the IDs from the c4d_symbols.h file, to get text string from c4d_strings.str file
    that as matches IDs.
    eg:
        Inside the c4d_strings.str file.
        dir: res / strings_us / c4d_strings.str

            IDS_TOOL_TITLE   "My Tool";
            IDS_TOOL_TEXT    "My Hello";
            etc...

        ------------------

        Inside the c4d_symbols.h file.
        dir: res / c4d_symbols.h

        enum
        {

            IDS_TOOL_TITLE   1001,
            IDS_TOOL_TEXT    1002,
            etc...

        _DUMMY_ELEMENT_
        };
    """
    def ID(self, id):
        """ Get String ID  """
        str_text = c4d.plugins.GeLoadString(id)
        return str_text
GGPS_IDS = Get_Global_Plugin_Strings()

# [ Main Plugin Name and Desc ]
PLUGIN_NAME = GGPS_IDS.ID(fcsID.IDS_SMP_NAME)
PLUGIN_DESC = GGPS_IDS.ID(fcsID.IDS_SMP_DESC)

# [ Main Plugin Icon Register ]
PLUGIN_ICON = os.path.join(PLUGIN_PATH['LIB'], 'Resources', 'images', "tool_icon_sng.png")

# [ Register Plugin Add-on Tool Tpyes to Cinema 4D ]
class C4D_RegisterToolTypes(object):
    """ Register C4D Plugin Data Types and Register Info PluginFlagsTpyes-Eg:(PF.OptionGear) """

    """
    Register Info Plugin Flags Tpyes
    _________________________________________________
    """
    # Plugin General Flags :
    HidePlugin = c4d.PLUGINFLAG_HIDE
    HideTool = c4d.PLUGINFLAG_HIDEPLUGINMENU
    RefreshPlugin = c4d.PLUGINFLAG_REFRESHALWAYS
    SmallNodePlugin = c4d.PLUGINFLAG_SMALLNODE
    # Command Plugin Flags:
    OptionGear = c4d.PLUGINFLAG_COMMAND_OPTION_DIALOG   # A info flag / Command has additional options. The user can access them through a small gadget.
    
    """
    Register C4D Plugin Data Types
    _________________________________________________
    """
    def RegisterCommand(self, id, str_name, infoflags, icon, helpInfo, dataClass):
        """ A CommandData Tool Plugin Register """
        plugin_Icon = c4d.bitmaps.BaseBitmap()
        plugin_Icon.InitWith(icon)        
        plugins.RegisterCommandPlugin(id=id,                    # Plugin register ID.
                                      str=str_name,             # This is for the Plugin Name to show in the Plugins lic4d.storage.
                                      info=infoflags,           # If you want a option button once you have a ExecuteOptionID in Data Class, then put in Flags info=c4d.PLUGINFLAG_COMMAND_OPTION_DIALOG | c4d.PLUGINFLAG_COMMAND_HOTKEY,
                                      icon=plugin_Icon,         # Plugin Icon Image.
                                      help=helpInfo,            # The plugin help info is on what the plugin does.
                                      dat=dataClass)            # The plugin data class.
        return True
regType = C4D_RegisterToolTypes()   # [ Register C4D Plugin Data Types Class ]
PF = C4D_RegisterToolTypes()        # [ PF.HidePlugin|PF.HideTool|PF.TagVis ]

# [ Plugin Message ]
def PluginMessage(id, data):
    """ 
    Implementing PluginMessage() and have your code run for example on C4DPL_PROGRAM_STARTED. 
    Also Build Menus. Start a function after c4d startup and open up. 
    """
    if id == c4d.C4DPL_COMMANDLINEARGS:
        LoadUserTrueSplitData(PLUGIN_PATH)
        # [ Print to the C4D Cosole ]        
        print(Initialized)


if __name__=='__main__':
    # [ Register Tools to Plugin to C4D ]
    regType.RegisterCommand(PLUGIN_ID, PLUGIN_NAME, PF.OptionGear, PLUGIN_ICON, PLUGIN_DESC, CMD(GGPS_IDS, PLUGIN_PATH))
